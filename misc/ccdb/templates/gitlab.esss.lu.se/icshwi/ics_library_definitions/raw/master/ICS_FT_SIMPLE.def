###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ## 
##  CCDB device types: ICS_xxxxx                                                            ##  
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Projekt: Cryo / CryogenicsLibrary / CryoLib ##
##                                                                                          ##  
##                               Flow Transmitter in g/s                                   ##  
##                                                                                          ##
##
############################         Version: 1.4             ################################
# Author:  Miklos Boros
# Date:    11-09-2021
# Version: v1.4
# Changes: 
# 1. This is a simplified Flow Transmitter, without the diffpressure shown on the OPI 
############################         Version: 1.3             ################################
# Author:  Miklos Boros
# Date:    27-05-2019
# Version: v1.3
# Changes: 
# 1. Variable Name Unification 
############################         Version: 1.2             ################################
# Author:  Miklos Boros
# Date:    28-02-2019
# Version: v1.2
# Changes: 
# 1. Major review, 
# 2. Indent,  unit standardization
############################         Version: 1.1             ################################
# Author:  Miklos Boros, Marino Vojneski 
# Date:    12-06-2018
# Version: v1.1
# Changes: 
# 1. Modified Alarm Signal section to be compatible with new format.
############################         Version: 1.0             ################################
# Author:  Miklos Boros 
# Date:    25-01-2018
# Version: v1.0
##############################################################################################


############################
#  STATUS BLOCK
############################ 
define_status_block()

#Operation modes
add_digital("OpMode_FreeRun",          PV_DESC="Operation Mode FreeRun", PV_ONAM="True",                PV_ZNAM="False")
add_digital("OpMode_Forced",           PV_DESC="Operation Mode Forced",  PV_ONAM="True",                PV_ZNAM="False")

#Inhibit signals (set by the PLC code, can't be changed by the OPI)
add_digital("Inhibit_Manual",          PV_DESC="Inhibit Manual Mode",    PV_ONAM="InhibitManual",       PV_ZNAM="AllowManual")
add_digital("Inhibit_Force",           PV_DESC="Inhibit Force Mode",     PV_ONAM="InhibitForce",        PV_ZNAM="AllowForce")
add_analog("TransmitterColor",         "INT",                            PV_DESC="Transmitter color")

#for OPI visualization
add_digital("EnableFreeRunBtn",        PV_DESC="Enable Free Run Button", PV_ONAM="True",                PV_ZNAM="False")
add_digital("EnableManualBtn",         PV_DESC="Enable Manual Button",   PV_ONAM="True",                PV_ZNAM="False")
add_digital("EnableForcedBtn",         PV_DESC="Enable Force Button",    PV_ONAM="True",                PV_ZNAM="False")
add_analog("ScaleLOW",                 "REAL",                           PV_DESC="Scale LOW",           PV_EGU="[PLCF#EGU]")
add_analog("ScaleHIGH",                "REAL",                           PV_DESC="Scale HIGH",          PV_EGU="[PLCF#EGU]")

#Transmitter value
add_analog("MeasValue",                "REAL",  ARCHIVE=True,                           PV_DESC="Flow Value",                   PV_EGU="[PLCF#EGU]")


#Alarm signals
add_major_alarm("LatchAlarm",          "Latching of alarms",             PV_ZNAM="True")
add_major_alarm("GroupAlarm",          "Group Alarm",                    PV_ZNAM="NominalState")
add_major_alarm("Underrange",          "Flow Underrange",                PV_ZNAM="NominalState")
add_major_alarm("Overrange",           "Flow Overrange",                 PV_ZNAM="NominalState")
add_major_alarm("HIHI",                "Flow HIHI",                      PV_ZNAM="NominalState")
add_minor_alarm("HI",                  "Flow HI",                        PV_ZNAM="NominalState")
add_minor_alarm("LO",                  "Flow LO",                        PV_ZNAM="NominalState")
add_major_alarm("LOLO",                "Flow LOLO",                      PV_ZNAM="NominalState")
add_major_alarm("IO_Error",            "IO_Error",                       PV_ZNAM="NominalState")
add_major_alarm("Module_Error",        "HW  Module Error",               PV_ZNAM="NominalState")
add_major_alarm("Param_Error",         "OPI Parameter Error",            PV_ZNAM="NominalState")

#Feedback
add_analog("FB_ForceValue",            "REAL",                           PV_DESC="Feedback Force Flow", PV_EGU="[PLCF#EGU]")
add_analog("FB_Limit_HIHI",            "REAL",                           PV_DESC="Feedback Limit HIHI", PV_EGU="[PLCF#EGU]")
add_analog("FB_Limit_HI",              "REAL",                           PV_DESC="Feedback Limit HI",   PV_EGU="[PLCF#EGU]")
add_analog("FB_Limit_LO",              "REAL",                           PV_DESC="Feedback Limit LO",   PV_EGU="[PLCF#EGU]")
add_analog("FB_Limit_LOLO",            "REAL",                           PV_DESC="Feedback Limit LOLO", PV_EGU="[PLCF#EGU]")


############################
#  COMMAND BLOCK
############################ 
define_command_block()

#OPI buttons
add_digital("Cmd_FreeRun",             PV_DESC="CMD: FreeRun Mode")
add_digital("Cmd_Force",               PV_DESC="CMD: Force Mode")
add_digital("Cmd_ForceVal",            PV_DESC="CMD: Force Value")

add_digital("Cmd_AckAlarm",            PV_DESC="CMD: Acknowledge Alarm")


############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

#Limits
add_analog("P_Limit_HIHI",             "REAL",                           PV_DESC="Limit HIHI",          PV_EGU="[PLCF#EGU]")
add_analog("P_Limit_HI",               "REAL",                           PV_DESC="Limit HI",            PV_EGU="[PLCF#EGU]")
add_analog("P_Limit_LO",               "REAL",                           PV_DESC="Limit LO",            PV_EGU="[PLCF#EGU]")
add_analog("P_Limit_LOLO",             "REAL",                           PV_DESC="Limit LOLO",          PV_EGU="[PLCF#EGU]")

#Forcing
add_analog("P_ForceValue",             "REAL",                           PV_DESC="Force Flow",          PV_EGU="[PLCF#EGU]")

